package id.co.nexsoft.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.cj.jdbc.ConnectionImpl;
import com.mysql.cj.jdbc.MysqlDataSource;

public class DatabaseConnection{

	private static Connection con = null;

	static {
		String url = "jdbc:mysql://localhost:3306/formative17";
		MysqlDataSource dataSource = new MysqlDataSource();
		dataSource.setURL(url);
		dataSource.setUser("root");
		dataSource.setPassword("90215121");
		 try {
			con = dataSource.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//	static {
//		String url = "jdbc:mysql://localhost:3306/formative17";
//		String user = "root";
//		String pass = "90215121";
//		try {
//			Class.forName("com.mysql.jdbc.Driver");
//			con = DriverManager.getConnection(url, user, pass);
//		} catch (ClassNotFoundException | SQLException e) {
//			e.printStackTrace();
//		}
//	}

	public static Connection getConnection() {
		return con;
	}
}