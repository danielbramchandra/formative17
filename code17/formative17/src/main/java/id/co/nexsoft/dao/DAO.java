package id.co.nexsoft.dao;

import java.sql.SQLException;
import java.util.List;

public interface DAO {
	public String addData(String query) throws SQLException;	
	public List<List<Object>>selectData(String query) throws SQLException;
	public List<String> listTable() throws SQLException;
	

}
