package id.co.nexsoft.formative17;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

//import id.co.nexsoft.formative16.MVCconfig;

public class WebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer{

	@Override
    protected Class<?>[] getRootConfigClasses()
    {
        // TODO Auto-generated method stub
        return null;
    }
  
    @Override
    protected Class<?>[] getServletConfigClasses()
    {
        // TODO Auto-generated method stub
        return new Class[] { MVCconfig.class };
    }
  
    @Override
    protected String[] getServletMappings()
    {
        // TODO Auto-generated method stub
        return new String[] { "/" };
    }
}
