package id.co.nexsoft.formative17;

import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import id.co.nexsoft.dao.DAOImplementation;



@Controller
@EnableWebMvc
public class WebController {
	@PostMapping("/addQuery")
	public ModelAndView addTable(String query) throws SQLException {
		ModelAndView mv = new ModelAndView();
		DAOImplementation impl = new DAOImplementation();
		String arr[] = query.split(" ", 2);
		String firstWord = arr[0];
		if (firstWord.matches("CREATE")||firstWord.matches("INSERT")) {			
			impl.addData(query);
			mv.addObject("data","Query Berhasil");
		} else if(firstWord.matches("SELECT")) {
			List<List<Object>> data = impl.selectData(query);
			mv.addObject("result",data);
		}
	
		mv.setViewName("result.jsp");
		return mv;
	}
}
