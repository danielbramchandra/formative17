package id.co.nexsoft.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import id.co.nexsoft.util.DatabaseConnection;

public class DAOImplementation implements DAO {

	static Connection con = DatabaseConnection.getConnection();

	@Override
	public String addData(String query) throws SQLException {
		// TODO Auto-generated method stub
		Statement ps = con.createStatement();
		ps.executeUpdate(query);
		return "Data Berhasil Ditambah";
	}

	@Override
	public List<List<Object>> selectData(String query) throws SQLException {

		List<List<Object>> row = new ArrayList<List<Object>>();

		Statement ps = con.createStatement();
		ResultSet result = ps.executeQuery(query);
		ResultSetMetaData rsmd = result.getMetaData();
		for (int i = 0; i < result.getRow(); i++) {
			List<Object> data = new ArrayList<Object>();
			for (int j = 0; j < rsmd.getColumnCount(); j++) {
				data.add(result.getObject(j));
			}
			row.add(data);
		}

		return row;
	}

	@Override
	public List<String> listTable() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public List<String> listTable() throws SQLException {
//		String queryListTable = "SHOW TABLES;";
//		Statement ps = con.createStatement();
//		ResultSet result = ps
//		return null;
//	}

}
